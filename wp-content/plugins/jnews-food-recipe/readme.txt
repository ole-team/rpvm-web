Change Log :

== 7.0.2 ==
- [IMPROVEMENT] Use .on() and .off()

== 7.0.1 ==
- [BUG] Fix contentURL video schema

== 7.0.0 ==
- [IMPROVEMENT] Compatible with JNews v7.0.0

== 6.0.1 ==
- [IMPROVEMENT] Only load the style and script file when needed

== 6.0.0 ==
- [IMPROVEMENT] Compatible with JNews v6.0.0

== 5.0.0 ==
- [IMPROVEMENT] Compatible with JNews v5.0.0

== 4.0.0 ==
- [IMPROVEMENT] Compatible with JNews v4.0.0

== 3.0.1 ==
- [IMPROVEMENT] Adding some input field for Google Structured Data

== 3.0.0 ==
- [IMPROVEMENT] Compatible with JNews v3.0.0

== 2.0.0 ==
- [IMPROVEMENT] Compatible with JNews v2.0.0

== 1.0.2 ==
- [BUG] Fix round number issue

== 1.0.1 ==
- [BUG] Fix wrong file name

== 1.0.0 ==
- First Release
