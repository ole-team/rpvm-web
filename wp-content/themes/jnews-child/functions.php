<?php

/**
 * Load parent theme style
 */
add_action( 'wp_enqueue_scripts', 'jnews_child_enqueue_parent_style' );

function jnews_child_enqueue_parent_style()
{
    wp_enqueue_style( 'jnews-parent-style', get_parent_theme_file_uri('/style.css'));
}
function get_post_primary_category( $post = 0, $taxonomy = 'category' ){
    if ( ! $post ) {
        $post = get_the_ID();
    }

    $terms        = get_the_terms( $post, $taxonomy );
    $primary_term = array();

    if ( $terms ) {
        $term_display = '';
        $term_slug    = '';
        $term_link    = '';
        if ( class_exists( 'WPSEO_Primary_Term' ) ) {
            $wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy, $post );
            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
            $term               = get_term( $wpseo_primary_term );
            if ( is_wp_error( $term ) ) {
                $term_display = $terms[0]->name;
                $term_slug    = $terms[0]->slug;
                $term_link    = get_term_link( $terms[0]->term_id );
            } else {
                $term_display = $term->name;
                $term_slug    = $term->slug;
                $term_link    = get_term_link( $term->term_id );
            }
        } else {
            $term_display = $terms[0]->name;
            $term_slug    = $terms[0]->slug;
            $term_link    = get_term_link( $terms[0]->term_id );
        }
        $primary_term['url']   = $term_link;
        $primary_term['slug']  = $term_slug;
        $primary_term['title'] = $term_display;
    }
    return $primary_term;
}
add_shortcode('videoDLM', function($atts){
    $html = '';
    $idDLM = $atts['id'];
    if(!empty($idDLM)){
        $html .= '<div class="video_wrapper_responsive vd-destacado"><div id="destacado-DLM" data-id="'.$idDLM.'"></div></div>';
    wp_enqueue_script('DMapi', 'https://api.dmcdn.net/all.js');
    }  
    return $html;
});
add_action('wp_head', 'adOPs_calls',12);
function adOPs_calls(){
    $adTagCall = '';
    $varcontrol = true;
    if(is_single()){
        if(has_tag('branded')){
            $varcontrol = false;
        }
    }
    if($varcontrol){
        $tags = '<script>';
        $tags .= '(function($) {
        window.googletag = window.googletag || {cmd: []};
        googletag.cmd.push(function() {
        var defaultSizesW = [728, 90];
        var defaultSizesL = [[728, 90], [728, 250]];
        var defaultSizesP = [[320,50],[300,250]];
        var defaultSizesS = [[300,250], [336,280]];
        var defaultSizesI = [[728, 90], [728, 250]];
        if($( window ).width()<720){
            defaultSizesW = [[320,100],[320,50],[300, 250]];
            defaultSizesL = [[320,100],[320,50]];
            defaultSizesP = [[300, 250],[300,600]];
            defaultSizesS = [[300, 250],[300,600],[336,280]];
            defaultSizesI = [[320,50],[300, 250],[336,280]];
        }';
        $tags .= 'googletag.defineSlot("/4923229/top_banner_atf_728x90_970x90_320x50_320x100", defaultSizesW , "leaderboard-1").addService(googletag.pubads());';
        if (is_front_page() or is_single()){
        $tags .= 'googletag.defineSlot("/4923229/midbanner_mid_320x50_300x250_336x280_728x90_728x250", defaultSizesW, "inlinebanner-1").addService(googletag.pubads());
            googletag.defineSlot("/4923229/sqrbanner6_mid_320x50_300x250_336x280_728x90_728x250", defaultSizesW, "inlinebanner-2").addService(googletag.pubads());';
        }else{
        $tags .= 'googletag.defineSlot("/4923229/sqrbanner1_atf_300x250_336x280_300x600", [300,250], "mediumrectangle-1").addService(googletag.pubads());
        googletag.defineSlot("/4923229/midbanner_mid_320x50_300x250_336x280_728x90_728x250", defaultSizesW, "inlinebanner-1").addService(googletag.pubads());
            googletag.defineSlot("/4923229/sqrbanner6_mid_320x50_300x250_336x280_728x90_728x250", defaultSizesW, "inlinebanner-2").addService(googletag.pubads());';
        }
        if(is_single()){
        $tags .= 'googletag.defineSlot("/4923229/nativeintext_1x1", [1, 1], "1x1_intext").addService(googletag.pubads());';
        }
        if(is_single()){
            $tempID = get_the_ID();
            $prm_ct = get_post_primary_category($tempID, 'category');
            $vertBrand = 'recetas';
            $section = $prm_ct['slug'];
            if(!empty($prm_ct)){
                $temp_section = $prm_ct['slug'];
                $section = $temp_section;
                $tags .= 'googletag.pubads().setTargeting("category", "'.$section.'");';
            }
            $tags .= 'googletag.pubads().setTargeting("content", "video");';
        }
        $tags .= 'googletag.pubads().setTargeting("site", "recetasparavivirmejor"); googletag.pubads().enableSingleRequest(); googletag.pubads().collapseEmptyDivs();
        googletag.enableServices();
        });';
        $tags .= '})(jQuery);';
        $tags .= '</script>';
        echo $tags;
    }
}
function media_add_author_dropdown()
{
    $scr = get_current_screen();
    if ( $scr->base !== 'upload' ) return;

    $author   = filter_input(INPUT_GET, 'author', FILTER_SANITIZE_STRING );
    $selected = (int)$author > 0 ? $author : '-1';
    $args = array(
        'show_option_none'   => 'All Authors',
        'name'               => 'author',
        'selected'           => $selected
    );
    wp_dropdown_users( $args );
}
add_action('restrict_manage_posts', 'media_add_author_dropdown');
function label_column($cols) {
    $cols["label"] = "Label";
    return $cols;
}

function label_value($column_name, $id) {
    echo  get_the_content($id);
}

function label_column_sortable($cols) {
    $cols["label"] = "name";
    return $cols;
} 
function hook_new_media_columns() {
    add_filter('manage_media_columns', 'label_column');
    add_action('manage_media_custom_column', 'label_value', 10, 2);
    add_filter('manage_upload_sortable_columns', 'label_column_sortable');
}

add_action('admin_init', 'hook_new_media_columns');